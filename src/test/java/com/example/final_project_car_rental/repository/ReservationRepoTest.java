package com.example.final_project_car_rental.repository;


import com.example.final_project_car_rental.model.Reservation;

import lombok.SneakyThrows;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.text.SimpleDateFormat;
import java.util.List;


@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(false)
public class ReservationRepoTest {

    @Autowired
    private ReservationRepo reservationRepo;

    @Test
    public void testAddNewReservation() {
        Reservation reservation = new Reservation();


        Reservation savedReservation = reservationRepo.save(reservation);

        Assertions.assertThat(savedReservation).isNotNull();
        Assertions.assertThat(savedReservation.getId()).isGreaterThan(0L);
    }

    @SneakyThrows
    @Test
    public void findReservationByDateFromBetween() {
        List<Reservation> result = reservationRepo.findReservationByDateFromBetween(
                new SimpleDateFormat("yyyy-MM-dd").parse("2021-09-01"),
                new SimpleDateFormat("yyyy-MM-dd").parse("2021-09-04"));

        Assertions.assertThat(result).hasSize(2);


    }

    @SneakyThrows
    @Test
    public void findReservationsByDateToBetween() {
        List<Reservation> result = reservationRepo.findReservationsByDateToBetween(
                new SimpleDateFormat("yyyy-MM-dd").parse("2021-09-01"),
                new SimpleDateFormat("yyyy-MM-dd").parse("2021-09-06"));

        Assertions.assertThat(result).hasSize(1);


    }

    @SneakyThrows
    @Test
    public void findReservationsByDateFromIsLessThanEqualAndDateToIsGreaterThanEqual() {
        List<Reservation> result = reservationRepo.findReservationsByDateFromIsLessThanEqualAndDateToIsGreaterThanEqual(
                new SimpleDateFormat("yyyy-MM-dd").parse("2021-10-09"),
                new SimpleDateFormat("yyyy-MM-dd").parse("2021-08-08"));

        Assertions.assertThat(result).hasSize(3);


    }


}
