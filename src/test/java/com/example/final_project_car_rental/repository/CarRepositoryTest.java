package com.example.final_project_car_rental.repository;


import com.example.final_project_car_rental.model.Car;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;


import java.util.Optional;


@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@Rollback(false)
public class CarRepositoryTest {
    @Autowired
    private CarRepo carRepo;

    @Test
    public void testAddNewCar() {
        Car car = new Car();
        car.setBrand("Saab");
        car.setModel("9000");
        car.setBodyType("Sedan");
        car.setYear(2018L);
        car.setColor("Blue");
        car.setMileage(122993L);
        car.setStatus("Available");
        car.setAmount(60L);
        Car savedCar = carRepo.save(car);
        System.out.println(savedCar);
        Assertions.assertThat(savedCar).isNotNull();
        Assertions.assertThat(savedCar.getId()).isGreaterThan(0L);
    }

    @Test
    @DisplayName("get all method")
    public void testListAll() {
        Iterable<Car> cars = carRepo.findAll();
        Assertions.assertThat(cars).hasSizeGreaterThan(0);
    }

    @Test
    @DisplayName("edit method")
    public void testEdit() {
        Long id = 1L;
        Optional<Car> optionalCar = carRepo.findById(id);
        Assertions.assertThat(optionalCar).isPresent();
        Car car = optionalCar.get();
        car.setMileage(10000L);
        carRepo.save(car);
        Optional<Car> optionalCar1 = carRepo.findById(id);
        Assertions.assertThat(optionalCar1).isPresent();
        Car editedCar = optionalCar1.get();
        Assertions.assertThat(editedCar.getMileage()).isEqualTo(10000L);
    }

    @Test
    @DisplayName("find by id method")
    public void testGet() {

        Long id = 1L;
        Optional<Car> optionalCar = carRepo.findById(id);
        Assertions.assertThat(optionalCar).isPresent();
    }

    @Test
    @DisplayName("delete method")
    public void testDelete() {
        Long id = 1L;
        carRepo.deleteById(id);
        Optional<Car> optionalCar = carRepo.findById(id);
        Assertions.assertThat(optionalCar).isNotPresent();
    }


}
