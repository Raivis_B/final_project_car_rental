INSERT INTO branch (branch_address)
VALUES ('Riga'),
       ('Bauska'),
       ('Ogre');

INSERT INTO Employee (first_name, last_name, position, branch_id)
VALUES ('Janis', 'Kamergrauzis', 'Manager', 1),
       ('Andrievs', 'Niedra', 'Service Manager', 1),
       ('Karlis', 'Zarins', 'Mechanic', 1),
       ('Juris', 'Gudrais', 'Agent', 1),
       ('Baiba', 'Sipeniece', 'Agent', 1),
       ('Jurgis', 'Liepnieks', 'Agent', 1),
       ('Andris', 'Ansons', 'Manager', 2),
       ('Mareks', 'Polis', 'Service Manager', 2),
       ('Uldis', 'Purins', 'Mechanic', 2),
       ('Marta', 'Karpa', 'Agent', 2),
       ('Ainars', 'Slesers', 'Agent', 2),
       ('Maigonis', 'Klibais', 'Agent', 2),
       ('Aleksejs', 'Kapturs', 'Manager', 3),
       ('Mareks', 'Sniegs', 'Service Manager', 3),
       ('Karlis', 'Lubaus', 'Mechanic', 3),
       ('Aigars', 'Smits', 'Agent', 3),
       ('Kaspars', 'Dzedulis', 'Agent', 3),
       ('Ansis', 'Egle', 'Agent', 3);

INSERT INTO Car (picture, brand, model, body_type, year, color, mileage, status, amount, branch_id)
values ('BMW_X5_BLACK.png', 'BMW', 'X5', 'SUV', 2019, 'Black', 23456, 'Available', 55, 1),
       ('BMW_X6_BLACK.jpg', 'BMW', 'X6', 'SUV', 2021, 'Black', 5333, 'Available', 75, 1),
       ('VW_GOLF_BLACK.jpg', 'Volkswagen', 'Golf', 'Hatchback', 2020, 'Black', 33456, 'Available', 60, 1),
       ('VW_GOLF_WHITE.jpg', 'Volkswagen', 'Golf', 'Hatchback', 2020, 'White', 66997, 'Available', 60, 1),
       ('VW_GOLF_RED.jpg', 'Volkswagen', 'Golf', 'Hatchback', 2021, 'Red', 111334, 'Available', 65, 2),
       ('OPEL_CORSA_BLUE.jpg', 'Opel', 'Corsa', 'Hatchback', 2016, 'Blue', 134118, 'Available', 45, 2),
       ('SEAT_ARONA_WHITE.jpg', 'Seat', 'Arona', 'SUV', 2020, 'White', 23249, 'Available', 60, 2),
       ('BMW_X5_BLACK.png', 'BMW', 'X5', 'SUV', 2019, 'Black', 45331, 'Available', 55, 2),
       ('BMW_X6_BLACK.jpg', 'BMW', 'X6', 'SUV', 2021, 'White', 9663, 'Unavailable', 75, 3),
       ('BMW_X5_WHITE.jpg', 'BMW', 'X5', 'SUV', 2017, 'White', 154167, 'Available', 55, 3),
       ('VW_GOLF_RED.jpg', 'Volkswagen', 'Golf', 'Hatchback', 2021, 'Red', 18045, 'Available', 60, 3),
       ('VW_GOLF_BLACK.jpg', 'Volkswagen', 'Golf', 'Hatchback', 2020, 'Black', 77400, 'Booked', 60, 3),
       ('VW_GOLF_BLACK.jpg', 'Volkswagen', 'Golf', 'Hatchback', 2020, 'Black', 84098, 'Available', 60, 3),
       ('BMW_X5_BLACK.png', 'BMW', 'X5', 'SUV', 2018, 'Black', 67456, 'Available', 55, 3),
       ('BMW_X6_BLACK.jpg', 'BMW', 'X6', 'SUV', 2021, 'Black', 10987, 'Available', 75, 3),
       ('VW_GOLF_BLACK.jpg', 'Volkswagen', 'Golf', 'Hatchback', 2020, 'Black', 67886, 'Available', 60, 3),
       ('VW_GOLF_WHITE.jpg', 'Volkswagen', 'Golf', 'Hatchback', 2020, 'White', 98532, 'Available', 60, 3),
       ('VW_GOLF_RED.jpg', 'Volkswagen', 'Golf', 'Hatchback', 2021, 'Red', 134534, 'Available', 65, 2),
       ('OPEL_CORSA_BLUE.jpg', 'Opel', 'Corsa', 'Hatchback', 2016, 'Blue', 198567, 'Available', 45, 2),
       ('SEAT_ARONA_WHITE.jpg', 'Seat', 'Arona', 'SUV', 2020, 'White', 45269, 'Available', 60, 2),
       ('BMW_X5_BLACK.png', 'BMW', 'X5', 'SUV', 2019, 'Black', 71331, 'Available', 55, 2),
       ('BMW_X6_WHITE.jpg', 'BMW', 'X6', 'SUV', 2021, 'White', 18345, 'Unavailable', 75, 3),
       ('BMW_X5_WHITE.jpg', 'BMW', 'X5', 'SUV', 2017, 'White', 111578, 'Available', 55, 1),
       ('VW_GOLF_RED.jpg', 'Volkswagen', 'Golf', 'Hatchback', 2021, 'Red', 44378, 'Available', 60, 1),
       ('VW_GOLF_BLACK.jpg', 'Volkswagen', 'Golf', 'Hatchback', 2020, 'Black', 81456, 'Booked', 60, 1),
       ('VW_GOLF_BLACK.jpg', 'Volkswagen', 'Golf', 'Hatchback', 2020, 'Black', 79371, 'Available', 60, 1);



INSERT INTO Users (address, userName, enabled, locked, name, password, surname, user_role)
values ('Riga', 'testAdmin@test.com', TRUE, FALSE, 'Test',
        '$2a$10$pyJyAJ3RFOVtwzPM83wHSui25iKDZAvgv/XiHDBr./4YDVEEUxdYu', 'Admin', 0),
       ('Riga', 'testCustomer@test.com', TRUE, FALSE, 'Test',
        '$2a$10$pyJyAJ3RFOVtwzPM83wHSui25iKDZAvgv/XiHDBr./4YDVEEUxdYu', 'Customer', 1),
       ('Riga', 'testEmployee@test.com', TRUE, FALSE, 'Test',
        '$2a$10$pyJyAJ3RFOVtwzPM83wHSui25iKDZAvgv/XiHDBr./4YDVEEUxdYu', 'Employee', 2);

