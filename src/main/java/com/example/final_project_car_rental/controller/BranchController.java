package com.example.final_project_car_rental.controller;

import com.example.final_project_car_rental.model.Branch;

import com.example.final_project_car_rental.model.exception.BranchLocationExistsException;
import com.example.final_project_car_rental.model.exception.BranchNotFoundException;
import com.example.final_project_car_rental.service.BranchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;


@Controller
@RequestMapping("/branches")
public class BranchController {


    private final BranchService branchService;

    @Autowired
    public BranchController(BranchService branchService) {
        this.branchService = branchService;

    }


    @GetMapping("/all")
    public String getAllBranches(Model model) {
        List<Branch> listBranches = branchService.getAllBranches();
        model.addAttribute("listBranches", listBranches);
        return "branch-list";
    }

    @GetMapping("/new")
    public String showNewForm(Model model) {
        model.addAttribute("branch", new Branch());
        model.addAttribute("pageTitle", "Add New Branch");
        return "branch-form";
    }

    @PostMapping("/save")
    public String saveBranch(Model model, Branch branch, RedirectAttributes ra) {
        try {
            branchService.addBranch(branch);
            ra.addFlashAttribute("message", "New branch is added! Time to celebrate!");
            return "redirect:/branches/all";
        } catch (BranchLocationExistsException exception) {
            model.addAttribute("warningMessage", exception.getMessage());
            return "branch-form";
        }
    }

    @GetMapping(value = "/delete/{id}") //delete mapping
    public String deleteThisBranch(@PathVariable("id") final Long id, RedirectAttributes ra) {
        try {
            branchService.deleteBranch(id);
            ra.addFlashAttribute("message", "The branch in with ID " + id + " has been deleted");
        } catch (BranchNotFoundException e) {
            ra.addFlashAttribute("warningMessage", e.getMessage());
        }
        return "redirect:/branches/all";
    }

    @GetMapping(value = "/edit/{id}") //put mapping
    public String updateThisBranch(@PathVariable("id") final Long id, RedirectAttributes ra, Model model) {
        try {
            Branch branch = branchService.getBranch(id);
            model.addAttribute("branch", branch);
            model.addAttribute("pageTitle", "Update Branch (ID: " + id + ")");
            return "branch-form";
        } catch (BranchNotFoundException e) {
            ra.addFlashAttribute("warningMessage", e.getMessage());
        }
        return "redirect:/branches/all";
    }


}
