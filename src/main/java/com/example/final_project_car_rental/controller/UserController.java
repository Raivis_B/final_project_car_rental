package com.example.final_project_car_rental.controller;


import com.example.final_project_car_rental.model.Reservation;
import com.example.final_project_car_rental.model.User;

import com.example.final_project_car_rental.model.exception.*;
import com.example.final_project_car_rental.service.UserService;
import lombok.AllArgsConstructor;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

@Controller
@AllArgsConstructor
@RequestMapping()
public class UserController {

    private final UserService userService;


    @GetMapping("/sign-in")
    String signIn() {

        return "redirect:/login";
    }

    @PostMapping("/sign-in")
    String signIp(User user) {

        userService.loadUserByUsername(user.getEmail());

        return "index";
    }

    @GetMapping("/sign-up")
    String signUp(Model model) {
        model.addAttribute("user", new User());

        return "sign-up";
    }

    @PostMapping("/sign-up")
    String signUp(User user, Model model) {

        try {
            userService.signUpUser(user);
            return "redirect:/login";
        } catch (UserEmailIsAlreadyTakenException exception) {
            model.addAttribute("warningMessage", exception.getMessage());
            return "sign-up";
        }


    }


    @GetMapping("/users/all")
    public String getAllUsers(Model model) {
        List<User> listUsers = userService.getAllUsers();
        model.addAttribute("listUsers", listUsers);
        return "users-list";
    }

    @GetMapping(value = "users/edit/{id}") //put mapping
    public String editThisUser(@PathVariable("id") final Long id, RedirectAttributes ra, Model model) {
        try {
            User user = userService.getUser(id);
            model.addAttribute("user", user);
            model.addAttribute("pageTitle", "Update Customer (ID: " + id + ")");
            return "user-form-update";
        } catch (CustomerNotFoundException e) {
            ra.addFlashAttribute("warningMessage", e.getMessage());
        }
        return "redirect:/users/all";
    }

    @PostMapping("/users/update")
    String updateUser(User user) {

        userService.updateUser(user);

        return "redirect:/users/all";
    }

//    Customer controller Logic Implementation

    @GetMapping("/users/new")
    public String showNewCustomerForm(Model model, Reservation reservation) {
        model.addAttribute("reservation", reservation);
        model.addAttribute("user", new User());

        return "reservation-flow-step2";
    }

    @PostMapping("/users/save")
    public String saveUser(Model model, User user, Reservation reservation, RedirectAttributes ra) {
        try {
            userService.signUpUser(user);
            ra.addFlashAttribute("message", "You have been successfully registered");
            model.addAttribute("reservation", reservation);
            model.addAttribute("user", user);
            return "reservation-flow-step3";
        } catch (UserEmailIsAlreadyTakenException exception) {
            model.addAttribute("warningMessage", exception.getMessage());
            model.addAttribute("reservation", reservation);
            model.addAttribute("user", user);
            return "reservation-flow-step2";
        }
    }


    @GetMapping(value = "/users/login/{email}")
    public String getUserByEmailWOReservation(Reservation reservation, Model model, @PathVariable("email") final String email) {
        try {
            User user = userService.getUserByEmailWOReservation(email, reservation);
            model.addAttribute("user", user);
            model.addAttribute("reservation", reservation);
            return "reservation-flow-step3";

        } catch (CustomerHasReservationException exception) {
            User user = userService.getUserByEmail(email);
            model.addAttribute("warningMessage", exception.getMessage());
            model.addAttribute("reservation", reservation);
            model.addAttribute("user", user);
            return "reservation-flow-step3";
        }
    }

    @GetMapping(value = "users/delete/{id}") //delete mapping
    public String deleteThisUser(@PathVariable("id") final Long id, RedirectAttributes ra, Model model) {
        try {
            userService.deleteUser(id);
            ra.addFlashAttribute("message", "Customer with ID " + id + " has been deleted");
        } catch (UserCantBeDeletedException e) {
            ra.addFlashAttribute("warningMessage", e.getMessage());
        }
        return "redirect:/users/all";
    }


    @GetMapping(value = "users/reservations/{id}")
    public String getUserReservations(@PathVariable("id") final Long id, Model model) {

        try {
            List<Reservation> listReservations = userService.getReservations(id);
            model.addAttribute("listReservations", listReservations);
            return "reservation-details";
        } catch (UserHasNoReservationsException exception) {
            model.addAttribute("infomessage", exception.getMessage());

        }
        return "reservation-details";


    }
}
