package com.example.final_project_car_rental.controller;

import com.example.final_project_car_rental.model.Branch;
import com.example.final_project_car_rental.model.Car;
import com.example.final_project_car_rental.model.exception.CarNotFoundException;
import com.example.final_project_car_rental.model.exception.DatesNotAllowedException;
import com.example.final_project_car_rental.model.exception.NoCarsAvailableException;
import com.example.final_project_car_rental.service.BranchService;
import com.example.final_project_car_rental.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

@Controller
@RequestMapping("/cars")
public class CarController {


    private final CarService carService;

    private final BranchService branchService;


    @Autowired
    public CarController(CarService carService, BranchService branchService) {
        this.carService = carService;
        this.branchService = branchService;

    }

    @PostMapping("/savewithoutpicture")
    public String addCar(Car car, RedirectAttributes ra) {
        carService.addCar(car);
        ra.addFlashAttribute("message", "The car with ID: " + car.getId() + " has been successfully updated!");
        return "redirect:/cars/all";
    }


    @PostMapping("/save")
    public String addCar(Car car, RedirectAttributes ra, @RequestParam("fileImage") MultipartFile multipartFile)
            throws IOException {
        String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
        car.setPicture(fileName);
        Car savedCar = carService.addCar(car);
        String uploadDir = "src/main/resources/static/image/car-pictures/" + savedCar.getId();
        Path uploadPath = Paths.get(uploadDir);

        if (!Files.exists(uploadPath)) {
            Files.createDirectories(uploadPath);
        }
        try (InputStream inputStream = multipartFile.getInputStream()) {
            Path filePath = uploadPath.resolve(fileName);
            Files.copy(inputStream, filePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new IOException("Could not save uploaded file: " + fileName);
        }
        ra.addFlashAttribute("message", "The car has been saved successfully");
        return "redirect:/cars/all";
    }


    @GetMapping("/all")
    public String getAllCars(Model model) {
        List<Car> cars = carService.getAllCars();
        model.addAttribute("listCars", cars);
        return "cars-list";
    }

    @GetMapping("/inbranch/{branchId}/{dateFrom}/{dateTo}")
    public String showCarsInBranch(Model model, @PathVariable("branchId") final Long branchId
            , @PathVariable("dateFrom") final @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateFrom
            , @PathVariable("dateTo") final @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateTo, RedirectAttributes ra) {

        try {
            List<Car> cars = carService.getAvailableCarsForPeriod(branchId, dateFrom, dateTo);

            Branch branch = branchService.getBranch(branchId);
            Long days = ChronoUnit.DAYS.between(dateFrom, dateTo);


            List<Branch> listBranches = branchService.getAllBranches();
            model.addAttribute("listCars", cars);
            model.addAttribute("listBranches", listBranches);
            model.addAttribute("chosenBranch", branch.getBranchAddress());
            model.addAttribute("pickUp", dateFrom);
            model.addAttribute("dropOff", dateTo);
            model.addAttribute("days", days);
            return "index";
        } catch (NoCarsAvailableException exception) {
            ra.addFlashAttribute("warningMessage", exception.getMessage());
            return "redirect:/home";
        } catch (DatesNotAllowedException exception) {
            ra.addFlashAttribute("warningMessage", exception.getMessage());
            return "redirect:/home";


        }
    }


    @GetMapping("/new")
    public String showNewForm(Model model) {
        List<Branch> listBranches = branchService.getAllBranches();
        model.addAttribute("car", new Car());
        model.addAttribute("listBranches", listBranches);
        model.addAttribute("pageTitle", "Add New Car");
        return "car-form-new";
    }


    @GetMapping(value = "{id}")
    public String getCar(Model model, @PathVariable final Long id) {
        Car car = carService.getCar(id);
        model.addAttribute("car", car);
        return "car";
    }


    @GetMapping(value = "/delete/{id}") //delete mapping
    public String deleteThisCar(@PathVariable("id") final Long id, RedirectAttributes ra) {
        try {
            carService.deleteCar(id);
            ra.addFlashAttribute("message", "The car with the ID " + id + " has been deleted");
        } catch (CarNotFoundException e) {
            ra.addFlashAttribute("warningMessage", e.getMessage());
        }
        return "redirect:/cars/all";
    }

    @GetMapping("/edit/{id}") //put mapping
    public String showEditForm(@PathVariable("id") Long id, Model model, RedirectAttributes ra) {
        try {
            Car car = carService.getCar(id);

            model.addAttribute("car", car);
            model.addAttribute("pageTitle", "Update Car (ID: " + id + ")");
            List<Branch> listBranches = branchService.getAllBranches();
            model.addAttribute("listBranches", listBranches);
            return "car-form-edit";
        } catch (CarNotFoundException e) {
            ra.addFlashAttribute("warningMessage", e.getMessage());
            return "redirect:/cars/all";
        }
    }

    @GetMapping("/changepicture/{id}") //put mapping
    public String showChangePictureForm(@PathVariable("id") Long id, Model model, RedirectAttributes ra) {
        try {
            Car car = carService.getCar(id);

            model.addAttribute("car", car);
            model.addAttribute("pageTitle", "Change Picture Car (ID: " + id + ")");
            List<Branch> listBranches = branchService.getAllBranches();
            model.addAttribute("listBranches", listBranches);
            return "car-form-change-picture";
        } catch (CarNotFoundException e) {
            ra.addFlashAttribute("warningMessage", e.getMessage());
            return "redirect:/cars/all";
        }
    }


}
