package com.example.final_project_car_rental.controller;


import com.example.final_project_car_rental.model.Customer;
import com.example.final_project_car_rental.model.Reservation;

import com.example.final_project_car_rental.model.exception.CustomerHasReservationException;
import com.example.final_project_car_rental.model.exception.CustomerNotFoundException;
import com.example.final_project_car_rental.model.exception.EmailIsAlreadyTakenException;
import com.example.final_project_car_rental.service.CustomerService;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;


import java.util.List;


@Controller
@RequestMapping("/customers")
public class CustomerController {


    private final CustomerService customerService;

    @Autowired
    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }


    @GetMapping("/new")
    public String showNewCustomerForm(Model model, Reservation reservation) {
        model.addAttribute("reservation", reservation);
        model.addAttribute("customer", new Customer());
        model.addAttribute("pageTitleCustomers", "Register");
        return "reservation-flow-step2";
    }

    @PostMapping("/save")
    public String saveCustomer(Model model, Customer customer, Reservation reservation, RedirectAttributes ra) {
        try {
            customerService.addCustomer(customer);
            ra.addFlashAttribute("message", "You have been successfully registered");
            model.addAttribute("reservation", reservation);
            model.addAttribute("customer", customer);
            return "reservation-flow-step3";
        } catch (EmailIsAlreadyTakenException exception) {
            model.addAttribute("warningMessage", exception.getMessage());
            model.addAttribute("pageTitleCustomers", "Register");
            model.addAttribute("reservation", reservation);
            model.addAttribute("customer", customer);
            return "reservation-flow-step2";
        }
    }

    @GetMapping(value = "/{email}")
    public String getCustomerByEmailWOReservation(Reservation reservation, Model model, @PathVariable("email") final String email) {
        try {
            Customer customer = customerService.getCustomerByEmailWOReservation(email, reservation);
            model.addAttribute("customer", customer);
            model.addAttribute("reservation", reservation);
            return "reservation-flow-step3";

        } catch (CustomerHasReservationException exception) {
            Customer customer = customerService.getCustomerByEmail(email);
            model.addAttribute("warningMessage", exception.getMessage());
            model.addAttribute("reservation", reservation);
            model.addAttribute("customer", customer);
            return "reservation-flow-step3";
        }
    }

    @GetMapping("/all")
    public String getAllCustomers(Model model) {
        List<Customer> listCustomers = customerService.getAllCustomers();
        model.addAttribute("listCustomers", listCustomers);
        return "customer-list";
    }

    @GetMapping(value = "/edit/{id}") //put mapping
    public String editThisCustomer(@PathVariable("id") final Long id, RedirectAttributes ra, Model model) {
        try {
            Customer customer = customerService.getCustomer(id);
            model.addAttribute("customer", customer);
            model.addAttribute("pageTitle", "Update Customer (ID: " + id + ")");
            return "customer-form";
        } catch (CustomerNotFoundException e) {
            ra.addFlashAttribute("warningMessage", e.getMessage());
        }
        return "redirect:/customers/all";
    }

    @PostMapping("/update")
    public String updateThisCustomer(Customer customer, RedirectAttributes ra) {
        customerService.updateCustomer(customer);
        ra.addFlashAttribute("message", "Customer's with ID: " + customer.getId() + " information has been updated successfully");
        return "redirect:/customers/all";
    }

    @GetMapping(value = "/delete/{id}") //delete mapping
    public String deleteThisCustomer(@PathVariable("id") final Long id, RedirectAttributes ra) {
        try {
            customerService.deleteCustomer(id);
            ra.addFlashAttribute("message", "Customer with ID " + id + " has been deleted");
        } catch (CustomerNotFoundException e) {
            ra.addFlashAttribute("warningMessage", e.getMessage());
        }
        return "redirect:/customers/all";
    }


}