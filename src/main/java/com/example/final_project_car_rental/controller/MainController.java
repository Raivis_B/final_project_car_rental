package com.example.final_project_car_rental.controller;

import com.example.final_project_car_rental.model.Branch;
import com.example.final_project_car_rental.service.BranchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/")
public class MainController {


    private final BranchService branchService;

    @Autowired
    public MainController(BranchService branchService) {
        this.branchService = branchService;
    }

    @GetMapping()
    public String showHomePage(Model model) {
        List<Branch> listBranches = branchService.getAllBranches();
        model.addAttribute("listBranches", listBranches);
        return "index";
    }

    @GetMapping("/home")
    public String goToHomePage(Model model) {
        List<Branch> listBranches = branchService.getAllBranches();
        model.addAttribute("listBranches", listBranches);
        return "index";
    }

}


