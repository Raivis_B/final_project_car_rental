package com.example.final_project_car_rental.controller;

import com.example.final_project_car_rental.model.*;
import com.example.final_project_car_rental.model.dto.ReservationDto;
import com.example.final_project_car_rental.model.exception.ReservationNotFoundException;
import com.example.final_project_car_rental.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.time.LocalDate;
import java.util.List;

@Controller
@RequestMapping("/reservations")
public class ReservationController {


    private final ReservationService reservationService;

    private final CustomerService customerService;

    private final BranchService branchService;

    private final CarService carService;

    private final UserService userService;

    @Autowired
    public ReservationController(ReservationService reservationService, CustomerService customerService
            , BranchService branchService, CarService carService, UserService userService) {
        this.reservationService = reservationService;
        this.customerService = customerService;
        this.branchService = branchService;
        this.carService = carService;
        this.userService = userService;
    }


    @GetMapping("/new/{id}/{pickUp}/{dropOf}/{days}")
    public String showNewReservationFormPrefilled(@PathVariable("id") final Long id
            , @PathVariable("pickUp") final @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateFrom
            , @PathVariable("dropOf") final @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate dateTo
            , @PathVariable("days") final Long days, Model model) {
        List<Customer> listCustomers = customerService.getAllCustomers();
        Car car = carService.getCar(id);

        ReservationDto reservationDto = new ReservationDto();
        reservationDto.setCar(car);
        reservationDto.setDateFrom(dateFrom);
        reservationDto.setDateTo(dateTo);
        reservationDto.setAmount(car.getAmount() * days);


        model.addAttribute("listCustomers", listCustomers);
        model.addAttribute("car", car);
        model.addAttribute("reservationDto", reservationDto);
        model.addAttribute("reservation", new Reservation());
        return "reservation-flow-step1";
    }

    @PostMapping("/save")
    public String saveReservation(Model model, Reservation reservation, RedirectAttributes ra) {
        try {
            reservationService.addNewReservation(reservation);
            model.addAttribute("message", "It was a success! Have a pleasant ride!");
            model.addAttribute("reservation", reservation);
            return "reservation-details";
        } catch (Exception exception) {
            ra.addFlashAttribute("warningMessage", exception.getMessage());
            model.addAttribute("pageTitleCustomers", "Register");
            return "reservation-flow-step1";
        }
    }

    @GetMapping(value = "{id}")
    public String getReservation(Model model, @PathVariable("id") final Long id) {
        Reservation reservation = reservationService.getReservation(id);
        model.addAttribute("reservation", reservation);
        return "reservation-details";
    }


    @GetMapping("/all")
    public String getAllReservations(Model model) {
        model.addAttribute("listReservations", reservationService.getAllReservations());
        return "reservation-list";
    }

    @GetMapping("/edit/{id}")
    public String editReservation(Model model, @PathVariable("id") final Long id, RedirectAttributes ra) {
        try {
            Reservation reservation = reservationService.getReservation(id);

            List<Branch> listBranches = branchService.getAllBranches();
            List<Car> listCars = carService.getAllCars();
            List<User> listUsers = userService.getAllUsers();

            model.addAttribute("reservation", reservation);
            model.addAttribute("pageTitle", "Update Reservation (ID: " + id + ")");
            model.addAttribute("listCars", listCars);
            model.addAttribute("listUsers", listUsers);
            model.addAttribute("listBranches", listBranches);
            return "reservation-form-edit";
        } catch (ReservationNotFoundException e) {
            ra.addFlashAttribute("warningMessage", e.getMessage());
            return "redirect:/reservations/all";
        }
    }

    @PostMapping("/update")
    public String updateReservation(Reservation reservation, RedirectAttributes ra) {
        reservationService.addNewReservation(reservation);
        ra.addFlashAttribute("message", "Reservation with ID: " + reservation.getId() + " information has been updated successfully");
        return "redirect:/reservations/all";
    }

    @GetMapping(value = "/delete/{id}") //delete mapping
    public String deleteReservation(@PathVariable("id") final Long id, RedirectAttributes ra) {
        try {
            reservationService.deleteReservation(id);
            ra.addFlashAttribute("message", "The reservation in with ID " + id + " has been deleted");
        } catch (ReservationNotFoundException e) {
            ra.addFlashAttribute("warningMessage", e.getMessage());
        }
        return "redirect:/reservations/all";
    }


}
