package com.example.final_project_car_rental;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FinalProjectCarRentalApplication {

	public static void main(String[] args) {
		SpringApplication.run(FinalProjectCarRentalApplication.class, args);
	}

}
