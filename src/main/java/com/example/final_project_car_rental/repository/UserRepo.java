package com.example.final_project_car_rental.repository;

import com.example.final_project_car_rental.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;



@Repository
public interface UserRepo extends CrudRepository<User, Long> {
    User findByEmailIgnoringCase(String email);


}
