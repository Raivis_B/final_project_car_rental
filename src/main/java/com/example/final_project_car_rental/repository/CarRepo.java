package com.example.final_project_car_rental.repository;

import com.example.final_project_car_rental.model.Car;
import com.example.final_project_car_rental.model.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface CarRepo extends CrudRepository<Car, Long> {
    List<Car> findByBranchId(Long branchId);

    List<Car> findByStatus(String status);


}
