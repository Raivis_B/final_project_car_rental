package com.example.final_project_car_rental.repository;

import com.example.final_project_car_rental.model.Car;
import com.example.final_project_car_rental.model.Reservation;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ReservationRepo extends CrudRepository<Reservation, Long> {

    List<Reservation> findReservationByDateFromBetween(Date dateFrom, Date dateTo);

    List<Reservation> findReservationsByDateToBetween(Date dateFrom, Date dateTo);

    List<Reservation> findReservationsByDateFromIsLessThanEqualAndDateToIsGreaterThanEqual(Date dateTo, Date dateFrom);


}
