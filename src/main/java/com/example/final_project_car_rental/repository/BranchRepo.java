package com.example.final_project_car_rental.repository;

import com.example.final_project_car_rental.model.Branch;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BranchRepo extends CrudRepository<Branch, Long> {
    Branch findByBranchAddressIgnoringCase(String branchAddress);
}
