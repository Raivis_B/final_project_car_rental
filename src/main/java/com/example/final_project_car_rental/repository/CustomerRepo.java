package com.example.final_project_car_rental.repository;

import com.example.final_project_car_rental.model.Customer;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerRepo extends CrudRepository<Customer, Long> {
    Customer findByEmailIgnoringCase(String email);
}
