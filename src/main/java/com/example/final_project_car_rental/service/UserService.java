package com.example.final_project_car_rental.service;

import com.example.final_project_car_rental.model.Reservation;
import com.example.final_project_car_rental.model.User;
import com.example.final_project_car_rental.model.exception.*;
import com.example.final_project_car_rental.repository.UserRepo;
import lombok.AllArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
@AllArgsConstructor
public class UserService implements UserDetailsService {

    private final UserRepo userRepo;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {

        final User user = userRepo.findByEmailIgnoringCase(email);

        if (user != null) {
            return user;
        } else {
            throw new UsernameNotFoundException(MessageFormat
                    .format("User with email {0} cannot be found.", email));
        }
    }

    public User signUpUser(User user) {

        if (userRepo.findByEmailIgnoringCase(user.getEmail()) != null) {
            throw new UserEmailIsAlreadyTakenException(user.getEmail());
        }


        final String encryptedPassword = bCryptPasswordEncoder.encode(user.getPassword());

        user.setPassword(encryptedPassword);

        final User createdUser = userRepo.save(user);


        return createdUser;

    }


    public List<User> getAllUsers() {
        return StreamSupport
                .stream(userRepo.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    public User getUser(Long id) {
        return userRepo.findById(id).orElseThrow(() ->
                new UserNotFoundException(id));
    }

    public User updateUser(User user) {

        userRepo.save(user);
        return user;

    }

    public User getUserByEmail(String email) {

        return userRepo.findByEmailIgnoringCase(email);
    }


    public User getUserByEmailWOReservation(String email, Reservation reservation) {

        if (userRepo.findByEmailIgnoringCase(email).getReservations().stream().collect(Collectors.toList())
                .stream()
                .filter(r -> r.getDateFrom().isBefore(reservation.getDateTo().plusDays(1))
                        && r.getDateTo().isAfter(reservation.getDateFrom().minusDays(1)))
                .collect(Collectors.toList()).size() > 0) {
            throw new CustomerHasReservationException(email);
        }
        return userRepo.findByEmailIgnoringCase(email);
    }

    public User deleteUser(Long id) {
        User user = getUser(id);

        if (user.getUserRole().name().equals("ADMIN")) {

            throw new UserCantBeDeletedException(id);
        }
        userRepo.delete(user);
        return user;

    }

    public List<Reservation> getReservations(Long id) {
        User user = getUser(id);

        List<Reservation> reservations = user.getReservations().stream().collect(Collectors.toList());


        if (reservations.size() == 0) {
            throw new UserHasNoReservationsException(id);
        }
        return reservations;
    }

}