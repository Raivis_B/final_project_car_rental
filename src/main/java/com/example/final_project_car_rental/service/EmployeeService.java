package com.example.final_project_car_rental.service;

import com.example.final_project_car_rental.model.Employee;
import com.example.final_project_car_rental.model.exception.EmployeeNotFoundException;
import com.example.final_project_car_rental.repository.EmployeeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class EmployeeService {

    private final EmployeeRepo employeeRepo;

    @Autowired
    public EmployeeService(EmployeeRepo employeeRepo) {
        this.employeeRepo = employeeRepo;
    }

    public Employee addEmployee(Employee employee) {
        return employeeRepo.save(employee);
    }

    public List<Employee> getAllEmployees() {
        return StreamSupport
                .stream(employeeRepo.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    public Employee getEmployee(Long id) {
        return employeeRepo.findById(id).orElseThrow(() ->
                new EmployeeNotFoundException(id));
    }

    public Employee deleteEmployee(Long id) {
        Employee employee = getEmployee(id);
        employeeRepo.delete(employee);
        return employee;
    }

    @Transactional
    public Employee editEmployee(Long id, Employee employee) {
        Employee employeeToEdit = getEmployee(id);
        employeeToEdit.setBranch(employee.getBranch());
        return employeeToEdit;
    }


}
