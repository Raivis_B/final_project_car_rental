package com.example.final_project_car_rental.service;


import com.example.final_project_car_rental.model.Customer;
import com.example.final_project_car_rental.model.Reservation;
import com.example.final_project_car_rental.model.exception.CustomerHasReservationException;
import com.example.final_project_car_rental.model.exception.CustomerNotFoundException;
import com.example.final_project_car_rental.model.exception.EmailIsAlreadyTakenException;
import com.example.final_project_car_rental.repository.CustomerRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

import java.util.List;
import java.util.stream.Collectors;

import java.util.stream.StreamSupport;

@Service
public class CustomerService {
    private final CustomerRepo customerRepo;



    @Autowired
    public CustomerService(CustomerRepo customerRepo) {
        this.customerRepo = customerRepo;

    }

    public Customer addCustomer(Customer customer) throws EmailIsAlreadyTakenException {
        if (customerRepo.findByEmailIgnoringCase(customer.getEmail()) != null) {
            throw new EmailIsAlreadyTakenException(customer.getEmail());
        }
        return customerRepo.save(customer);
    }

    public Customer updateCustomer(Customer customer)  {
        return customerRepo.save(customer);
    }

    public List<Customer> getAllCustomers() {
        return StreamSupport
                .stream(customerRepo.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    public Customer getCustomer(Long id) {
        return customerRepo.findById(id).orElseThrow(() ->
                new CustomerNotFoundException(id));
    }

    public Customer getCustomerByEmail(String email) {
        return customerRepo.findByEmailIgnoringCase(email);
    }

    public Customer getCustomerByEmailWOReservation(String email, Reservation reservation) {

        if (customerRepo.findByEmailIgnoringCase(email).getReservations().stream().collect(Collectors.toList())
                .stream()
                .filter(r -> r.getDateFrom().isBefore(reservation.getDateTo().plusDays(1))
                        && r.getDateTo().isAfter(reservation.getDateFrom().minusDays(1)))
                .collect(Collectors.toList()).size() > 0) {
            throw new CustomerHasReservationException(email);
        }
        return customerRepo.findByEmailIgnoringCase(email);
    }


    public Customer deleteCustomer(Long id) {
        Customer customer = getCustomer(id);
        customerRepo.delete(customer);
        return customer;
    }


    @Transactional
    public Customer editCustomer(Long id, Customer customer) {
        Customer customerToEdit = getCustomer(id);
        customerToEdit.setFirstName(customer.getFirstName());
        return customerToEdit;
    }

}
