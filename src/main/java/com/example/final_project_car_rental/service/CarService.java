package com.example.final_project_car_rental.service;

import com.example.final_project_car_rental.model.Car;
import com.example.final_project_car_rental.model.exception.CarNotFoundException;
import com.example.final_project_car_rental.model.exception.DatesNotAllowedException;
import com.example.final_project_car_rental.model.exception.NoCarsAvailableException;
import com.example.final_project_car_rental.repository.CarRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.time.LocalDate;

import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CarService {

    private final CarRepo carRepo;


    @Autowired
    public CarService(CarRepo carRepo) {
        this.carRepo = carRepo;
    }

    public Car addCar(Car car) {
        return carRepo.save(car);
    }

    public List<Car> getAllCars() {
        return (List<Car>) carRepo.findAll();
    }

    public Car getCar(Long id) {
        return carRepo.findById(id).orElseThrow(() ->
                new CarNotFoundException(id));
    }


    public Car deleteCar(Long id) {
        Car car = getCar(id);
        carRepo.delete(car);
        return car;
    }

    public List<Car> getAvailableCarsForPeriod(Long branchId, LocalDate dateFrom, LocalDate dateTo) {

        LocalDate dateNow = LocalDate.now();
       Long days = ChronoUnit.DAYS.between(dateFrom, dateTo);

        if(dateFrom.isBefore(dateNow) || dateFrom.isAfter(dateTo) || days > 30 ){
            throw new DatesNotAllowedException(dateNow,  dateTo, dateFrom, days);
        }

        List<Car> cars = carRepo.findByBranchId(branchId).stream().filter(car -> car.getStatus()
                .equals("Available")).collect(Collectors.toList());

        List<Car> reservedCars = cars.stream().flatMap(car -> car.getReservations().stream())
                .collect(Collectors.toList()).stream().collect(Collectors.toList())
                .stream()
                .filter(r -> r.getDateFrom().isBefore(dateTo.plusDays(1)) && r.getDateTo().isAfter(dateFrom.minusDays(1)))
                .collect(Collectors.toList()).stream().map(reservation -> reservation.getCar())
                .collect(Collectors.toList());
        cars.removeAll(reservedCars);
        if (cars.size() == 0) {
            throw new NoCarsAvailableException(branchId);
        }

        return cars;
    }


//    @Transactional
//    public Car editCar(Long id, Car car) {
//        Car carToEdit = getCar(id);
//        carToEdit.setBranch(car.getBranch());
//        return carToEdit;
//    }


}
