package com.example.final_project_car_rental.service;


import com.example.final_project_car_rental.model.Reservation;

import com.example.final_project_car_rental.model.exception.ReservationNotFoundException;
import com.example.final_project_car_rental.repository.ReservationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ReservationService {

    private final ReservationRepo reservationRepo;

    @Autowired
    public ReservationService(ReservationRepo reservationRepo) {
        this.reservationRepo = reservationRepo;
    }

    public Reservation addNewReservation(Reservation reservation) {
        return reservationRepo.save(reservation);
    }

    public List<Reservation> getAllReservations() {
        return (List<Reservation>) reservationRepo.findAll();
    }

    public Reservation getReservation(Long id) {
        return reservationRepo.findById(id).orElseThrow(() ->
                new ReservationNotFoundException(id));
    }

    public Reservation deleteReservation(Long id) {
        Reservation reservation = getReservation(id);
        reservationRepo.delete(reservation);
        return reservation;
    }


}
