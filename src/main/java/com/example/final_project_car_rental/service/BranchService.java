package com.example.final_project_car_rental.service;

import com.example.final_project_car_rental.model.Branch;
import com.example.final_project_car_rental.model.Car;
import com.example.final_project_car_rental.model.Employee;
import com.example.final_project_car_rental.model.exception.*;
import com.example.final_project_car_rental.repository.BranchRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class BranchService {

    private final BranchRepo branchRepo;
    private final EmployeeService employeeService;
    private final CarService carService;

    @Autowired
    public BranchService(BranchRepo branchRepo, EmployeeService employeeService, CarService carService) {
        this.branchRepo = branchRepo;
        this.employeeService = employeeService;
        this.carService = carService;
    }

    public Branch addBranch(Branch branch) {
        if (branchRepo.findByBranchAddressIgnoringCase(branch.getBranchAddress()) != null) {
            throw new BranchLocationExistsException(branch.getBranchAddress());
        }
        return branchRepo.save(branch);
    }


    public List<Branch> getAllBranches() {
        return StreamSupport
                .stream(branchRepo.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }

    public Branch getBranch(Long id) {
        return branchRepo.findById(id).orElseThrow(() ->
                new BranchNotFoundException(id));
    }


    public Branch deleteBranch(Long id) {
        Branch branch = getBranch(id);
        branchRepo.delete(branch);
        return branch;
    }

    @Transactional
    public Branch editBranch(Long id, Branch branch) {
        Branch branchToEdit = getBranch(id);
        branchToEdit.setBranchAddress(branch.getBranchAddress());
        return branchToEdit;
    }

    @Transactional
    public Branch addEmployeeToBranch(Long branchId, Long employeeId) {
        Branch branch = getBranch(branchId);
        Employee employee = employeeService.getEmployee(employeeId);
        if (Objects.nonNull(employee.getBranch())) {
            throw new EmployeeIsAlreadyAssignedException(employeeId, employee.getBranch().getId());
        }
        branch.addEmployee(employee);
        return branch;
    }

    @Transactional
    public Branch removeEmployeeFromBranch(Long branchId, Long employeeId) {
        Branch branch = getBranch(branchId);
        Employee employee = employeeService.getEmployee(employeeId);
        branch.removeEmployee(employee);
        return branch;
    }

    @Transactional
    public Branch addCarToBranch(Long branchId, Long carId) {
        Branch branch = getBranch(branchId);
        Car car = carService.getCar(carId);
        if (Objects.nonNull(car.getBranch())) {
            throw new CarIsAlreadyAssignedException(carId, car.getBranch().getId());
        }
        branch.addCar(car);
        return branch;
    }

}
