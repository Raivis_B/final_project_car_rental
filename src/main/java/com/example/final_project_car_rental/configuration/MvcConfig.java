package com.example.final_project_car_rental.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.nio.file.Path;
import java.nio.file.Paths;

@Configuration
public class MvcConfig implements WebMvcConfigurer {

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        Path carUploadDir = Paths.get("src/main/resources/static/image/car-pictures/");
        String carUploadPath = carUploadDir.toFile().getPath();

        registry.addResourceHandler("car-pictures/**").addResourceLocations("file:" + carUploadPath + "/");
    }
}
