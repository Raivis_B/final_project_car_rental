package com.example.final_project_car_rental.security;

import com.example.final_project_car_rental.service.UserService;
import lombok.AllArgsConstructor;

import org.springframework.context.annotation.Configuration;

import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@AllArgsConstructor
//@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final UserService userService;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;



    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http.authorizeRequests()
                .antMatchers("/sign-up/**", "/sign-in/**")
                .permitAll()
                .antMatchers("/*/all").authenticated()
                .antMatchers("/users/login/**").authenticated()
                .antMatchers("/*/delete/**").hasAuthority("ADMIN")
                .antMatchers("/h2").permitAll()
                .antMatchers("/login*").permitAll()
                .antMatchers("/users/**").permitAll()
                .anyRequest().permitAll()
                .and()
                .formLogin()
//                .loginPage("/sign-in")
                .and()
                .httpBasic()
                .and()
                .logout()
                .logoutSuccessUrl("/home")
                .and()
                .headers().frameOptions().disable()
                .and()
                .csrf().disable();


    }
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(userService)
                .passwordEncoder(bCryptPasswordEncoder);
    }

}
