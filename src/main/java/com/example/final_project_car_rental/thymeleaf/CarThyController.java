//package com.example.final_project_car_rental.thymeleaf;
//
//import com.example.final_project_car_rental.repository.CarRepo;
//import lombok.RequiredArgsConstructor;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.ModelMap;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.RequestMapping;
//
//@Controller
//@RequiredArgsConstructor
//@RequestMapping("/carrental")
//public class CarThyController {
//    private final CarRepo carRepo;
//
//    @GetMapping("/cars")
//    public  String showAllCars(final ModelMap modelMap){
//        modelMap.addAttribute("carslist", carRepo.findAll());
//        return "cars-list";
//    }
//
//    @GetMapping("/cars/{available}")
//    public  String showAllCarsAvailable(final ModelMap modelMap, String available){
//        modelMap.addAttribute("carslist", carRepo.findByStatus("Available"));
//        return "cars-list";
//    }
//
//}
