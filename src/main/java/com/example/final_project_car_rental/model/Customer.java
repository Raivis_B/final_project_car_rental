package com.example.final_project_car_rental.model;

import com.example.final_project_car_rental.model.dto.CustomerDto;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long Id;
    @NotEmpty
    @Size(min = 2, message = "First name should have at least two character")
    private String firstName;
    @NotEmpty
    @Size(min = 2, message = "Last name should have at least two character")
    private String lastName;
    @NotEmpty
    @Email
    private String email;
    private String address;

    @OneToMany(
            cascade = CascadeType.ALL
    )
    @JoinColumn(name = "customer_id")
    @ToString.Exclude
    private List<Reservation> reservations = new ArrayList<>();

    public void addReservation(Reservation reservation) {
        reservations.add(reservation);
    }

    public void removeReservation(Reservation reservation) {
        reservations.remove(reservation);
    }

//    public static Customer from(CustomerDto customerDto) {
//        Customer customer = new Customer();
//        customer.setFirstName(customerDto.getFirstName());
//        customer.setLastName(customerDto.getLastName());
//        customer.setEmail(customerDto.getEmail());
//        customer.setAddress(customerDto.getAddress());
//        return customer;
//    }


}
