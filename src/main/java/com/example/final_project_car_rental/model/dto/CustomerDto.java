package com.example.final_project_car_rental.model.dto;

import com.example.final_project_car_rental.model.Customer;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;


@Data
public class CustomerDto {
    private Long Id;
    @NotEmpty
    @Size(min = 2, message = "First name should have at least two characters")
    private String firstName;
    @NotEmpty
    @Size(min = 2, message = "Last name should have at least two characters")
    private String lastName;
    @NotEmpty(message = "Email is a mandatory field! Please provide your email!")
    @Email(message = "Please provide correct email!")
    private String email;
    @NotEmpty
    @Size(min = 2, message = "Address should have at least two characters")
    private String address;

    public static CustomerDto from(Customer customer) {
        CustomerDto customerDto = new CustomerDto();
        customerDto.setId(customer.getId());
        customerDto.setFirstName(customer.getFirstName());
        customerDto.setLastName(customer.getLastName());
        customerDto.setEmail(customer.getEmail());
        customerDto.setAddress(customer.getAddress());
        return customerDto;

    }

}
