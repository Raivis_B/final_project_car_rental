package com.example.final_project_car_rental.model.dto;

import com.example.final_project_car_rental.model.Branch;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;


@Data
public class BranchDto {
    private Long id;
    private String branchAddress;
//    private List<EmployeeDto> employeesDto = new ArrayList<>();
    private List<CarDto> carsDto = new ArrayList<>();

    public static BranchDto from(Branch branch) {
        BranchDto branchDto = new BranchDto();
        branchDto.setId(branch.getId());
        branchDto.setBranchAddress(branch.getBranchAddress());

        return branchDto;
    }
}
