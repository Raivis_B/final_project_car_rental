package com.example.final_project_car_rental.model.dto;

import com.example.final_project_car_rental.model.Branch;
import com.example.final_project_car_rental.model.Car;
import com.example.final_project_car_rental.model.Customer;
import com.example.final_project_car_rental.model.Reservation;
import lombok.Data;

import java.time.LocalDate;

@Data
public class ReservationDto {
    private Long id;
    private LocalDate dateOfBooking;
    private Customer customer;
    private Car car;
    private LocalDate dateFrom;
    private LocalDate dateTo;
    private Branch branch;
    private String returnDepartment;
    private Long amount;


    public static ReservationDto from(Reservation reservation) {
        ReservationDto reservationDto = new ReservationDto();
        reservationDto.setId(reservation.getId());
        reservationDto.setDateOfBooking(reservation.getDateOfBooking());
//        reservationDto.setCustomer(reservation.getCustomer());
        reservationDto.setCar(reservation.getCar());
        reservationDto.setDateFrom(reservation.getDateFrom());
        reservationDto.setDateTo(reservation.getDateTo());
        reservationDto.setBranch(reservation.getBranch());
        reservationDto.setReturnDepartment(reservation.getReturnDepartment());
        reservationDto.setAmount(reservation.getAmount());

        return reservationDto;

    }


}
