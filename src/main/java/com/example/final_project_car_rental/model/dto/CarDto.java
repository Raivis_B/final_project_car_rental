package com.example.final_project_car_rental.model.dto;


import com.example.final_project_car_rental.model.Branch;
import com.example.final_project_car_rental.model.Car;
import lombok.Data;

import java.util.Objects;

@Data
public class CarDto {
    private Long id;
    private String brand;
    private String model;
    private String bodyType;
    private Long year;
    private String color;
    private Long mileage;
    private String status;
    private Long amount;
    private Branch branch;
    private PlainBranchDto plainBranchDto;


    public static CarDto from(Car car) {
        CarDto carDto = new CarDto();
        carDto.setId(car.getId());
        carDto.setBrand(car.getBrand());
        carDto.setModel(car.getModel());
        carDto.setBodyType(car.getBodyType());
        carDto.setYear(car.getYear());
        carDto.setColor(car.getColor());
        carDto.setMileage(car.getMileage());
        carDto.setStatus(car.getStatus());
        carDto.setAmount(car.getAmount());
        if (Objects.nonNull(car.getBranch())) {
            carDto.setPlainBranchDto(PlainBranchDto.from(car.getBranch()));
        }
        return carDto;

    }


}


