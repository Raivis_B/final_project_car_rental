package com.example.final_project_car_rental.model.dto;

import com.example.final_project_car_rental.model.Employee;
import lombok.Data;

import java.util.Objects;

@Data
public class EmployeeDto {
    private Long id;
    private String firstName;
    private String lastName;
    private String position;
    private PlainBranchDto plainBranchDto;


    public static EmployeeDto from(Employee employee) {
        EmployeeDto employeeDto = new EmployeeDto();
        employeeDto.setId(employee.getId());
        employeeDto.setFirstName(employee.getFirstName());
        employeeDto.setLastName(employee.getLastName());
        employeeDto.setPosition(employee.getPosition());
        if (Objects.nonNull(employee.getBranch())) {
            employeeDto.setPlainBranchDto(PlainBranchDto.from(employee.getBranch()));
        }
        return employeeDto;

    }
}
