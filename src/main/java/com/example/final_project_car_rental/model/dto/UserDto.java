package com.example.final_project_car_rental.model.dto;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Id;

@Data
public class UserDto {
    @Id
    private String username;

    @Column(name = "password")
    private String password;
    private String matchingPassword;

    @Column(name = "role")
    private String role;

}
