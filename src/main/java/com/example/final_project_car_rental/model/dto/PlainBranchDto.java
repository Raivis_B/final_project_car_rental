package com.example.final_project_car_rental.model.dto;

import com.example.final_project_car_rental.model.Branch;
import lombok.Data;

@Data
public class PlainBranchDto {
    private Long id;
    private String branchAddress;

    public static PlainBranchDto from(Branch branch) {
        PlainBranchDto plainBranchDto = new PlainBranchDto();
        plainBranchDto.setId(branch.getId());
        plainBranchDto.setBranchAddress(branch.getBranchAddress());
        return plainBranchDto;
    }

}
