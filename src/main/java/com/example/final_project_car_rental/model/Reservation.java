package com.example.final_project_car_rental.model;

import com.example.final_project_car_rental.model.dto.ReservationDto;
import lombok.Data;
import lombok.ToString;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
@Table(name = "Reservation")
public class Reservation {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
   @CreationTimestamp
    private LocalDate dateOfBooking;
    @ManyToOne
    @ToString.Exclude
    private Customer customer;
    @ManyToOne
    @ToString.Exclude
    private User user;
    @ManyToOne
    @ToString.Exclude
    @JoinColumn(name = "car_id")
    private Car car;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateFrom;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateTo;
    @ManyToOne
    @ToString.Exclude
    private Branch branch;
    private String returnDepartment;
    private Long amount;




    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    public static Reservation from(ReservationDto reservationDto) {
        Reservation reservation = new Reservation();
        reservation.setId(reservationDto.getId());
        reservation.setDateOfBooking(reservationDto.getDateOfBooking());
        reservation.setCustomer(reservationDto.getCustomer());
        reservation.setCar(reservationDto.getCar());
        reservation.setDateFrom(reservationDto.getDateFrom());
        reservation.setDateTo(reservationDto.getDateTo());
        reservation.setBranch(reservationDto.getBranch());
        reservation.setReturnDepartment(reservationDto.getReturnDepartment());
        reservation.setAmount(reservationDto.getAmount());

        return reservation;

    }


}
