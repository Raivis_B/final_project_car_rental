package com.example.final_project_car_rental.model;

public enum UserRole {

    ADMIN, CUSTOMER, EMPLOYEE
}
