package com.example.final_project_car_rental.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Entity
public class Rental {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;
    private String domain;
    private String contactAddress;
    private String owner;
    private String logo;
    @OneToMany(
            cascade = CascadeType.ALL
    )
    @JoinColumn(name = "rentalId")
    private List<Branch> branches = new ArrayList<>();


}
