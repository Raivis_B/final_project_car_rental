package com.example.final_project_car_rental.model.exception;

import java.text.MessageFormat;

public class UserEmailIsAlreadyTakenException extends RuntimeException{
    public UserEmailIsAlreadyTakenException(final String email) {
        super(MessageFormat.format("Email: {0} is already registered! Please sign in to proceed!", email));
    }
}
