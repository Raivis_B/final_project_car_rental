package com.example.final_project_car_rental.model.exception;

import java.text.MessageFormat;

public class UserNotFoundException extends RuntimeException{
    public UserNotFoundException(final Long id) {
        super(MessageFormat.format("Could not find user with id: {0}", id));
    }
}
