package com.example.final_project_car_rental.model.exception;


public class ApiException extends RuntimeException{
    private static final long serialVersionUID = 1L;

    public ApiException(String message){
        super(message);
    }
}
