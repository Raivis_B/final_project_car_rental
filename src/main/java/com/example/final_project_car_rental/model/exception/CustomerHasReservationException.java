package com.example.final_project_car_rental.model.exception;

import java.text.MessageFormat;

public class CustomerHasReservationException extends RuntimeException{
    public CustomerHasReservationException(String email) {
        super(MessageFormat.format("You already have reservation on this period!" +
                " It is allowed to only have one reservation per period!", email));
    }
}
