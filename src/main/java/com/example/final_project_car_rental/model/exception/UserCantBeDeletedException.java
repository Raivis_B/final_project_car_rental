package com.example.final_project_car_rental.model.exception;

import java.text.MessageFormat;

public class UserCantBeDeletedException extends RuntimeException{
    public UserCantBeDeletedException(final Long id) {
        super(MessageFormat.format("User with the ID {0} can't be deleted, because it's an ADMIN!", id));
    }
}
