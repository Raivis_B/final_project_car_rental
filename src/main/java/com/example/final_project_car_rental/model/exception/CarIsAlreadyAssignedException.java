package com.example.final_project_car_rental.model.exception;

import java.text.MessageFormat;

public class CarIsAlreadyAssignedException extends RuntimeException {
    public CarIsAlreadyAssignedException(final Long carId, final Long branchId) {
        super(MessageFormat.format("Car: {0} is already assigned to branch: {1}", carId, branchId));
    }
}
