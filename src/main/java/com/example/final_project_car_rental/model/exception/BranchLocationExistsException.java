package com.example.final_project_car_rental.model.exception;

import java.text.MessageFormat;

public class BranchLocationExistsException extends RuntimeException{
    public BranchLocationExistsException(final String branchAddress) {
        super(MessageFormat.format("We already have a branch in {0}!", branchAddress));
    }
}
