package com.example.final_project_car_rental.model.exception;

import java.text.MessageFormat;

public class EmployeeNotFoundException extends RuntimeException {
    public EmployeeNotFoundException(final Long id) {
        super(MessageFormat.format("Could not find employee with id: {0}", id));
    }
}
