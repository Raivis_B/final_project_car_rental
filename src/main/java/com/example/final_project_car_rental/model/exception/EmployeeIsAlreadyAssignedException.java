package com.example.final_project_car_rental.model.exception;

import java.text.MessageFormat;

public class EmployeeIsAlreadyAssignedException extends RuntimeException{
    public EmployeeIsAlreadyAssignedException(final Long employeeId, final Long branchId) {
        super(MessageFormat.format("Employee: {0} is already assigned to branch: {1}", employeeId, branchId));
    }
}
