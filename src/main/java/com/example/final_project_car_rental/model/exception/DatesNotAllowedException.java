package com.example.final_project_car_rental.model.exception;

import org.apache.tomcat.jni.Local;

import java.text.MessageFormat;
import java.time.LocalDate;

public class DatesNotAllowedException extends RuntimeException{
    public DatesNotAllowedException(final LocalDate dateNow, final LocalDate dateFrom, final LocalDate dateTo, final Long days) {
        super(MessageFormat.format("You entered illegal dates. Either your pick-up date {1} is before today" +
                " {0} or return date {2} is before pick-up date, or reservation period ({3} days) is greater than 30 days or negative. Please enter valid dates.", dateNow, dateFrom,dateTo, days ));
    }
}
