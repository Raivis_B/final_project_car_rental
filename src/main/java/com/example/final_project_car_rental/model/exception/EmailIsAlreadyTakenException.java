package com.example.final_project_car_rental.model.exception;

import java.text.MessageFormat;

public class EmailIsAlreadyTakenException extends RuntimeException{
    public EmailIsAlreadyTakenException(final String email) {
        super(MessageFormat.format("Email: {0} is already registered! Please login to proceed!", email));
    }
}
