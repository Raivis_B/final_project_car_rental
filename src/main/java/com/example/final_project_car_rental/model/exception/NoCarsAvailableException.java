package com.example.final_project_car_rental.model.exception;

import java.text.MessageFormat;

public class NoCarsAvailableException extends RuntimeException{
    public NoCarsAvailableException(Long branchId) {
        super(MessageFormat.format("There are no cars available for given period. We are sorry!", branchId));
    }
}
