package com.example.final_project_car_rental.model.exception;

import java.text.MessageFormat;

public class ReservationNotFoundException extends RuntimeException{
    public ReservationNotFoundException(final Long id) {
        super(MessageFormat.format("Could not find reservation with id: {0}", id));
    }
}
