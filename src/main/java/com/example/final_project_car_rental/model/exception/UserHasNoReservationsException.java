package com.example.final_project_car_rental.model.exception;

import java.text.MessageFormat;

public class UserHasNoReservationsException extends RuntimeException{
    public UserHasNoReservationsException(final Long id) {
        super(MessageFormat.format("You don't have any reservations yet. It's time to book a car!", id));
    }
}
