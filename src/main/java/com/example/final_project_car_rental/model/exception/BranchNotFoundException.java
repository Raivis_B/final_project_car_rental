package com.example.final_project_car_rental.model.exception;

import java.text.MessageFormat;

public class BranchNotFoundException extends RuntimeException{
    public BranchNotFoundException(final Long id) {
        super(MessageFormat.format("Could not find branch with id: {0}", id));
    }
}
