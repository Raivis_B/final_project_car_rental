package com.example.final_project_car_rental.model;


import lombok.Data;

import lombok.ToString;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Table(name = "Branch")
public class Branch {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String branchAddress;

    @OneToMany(fetch = FetchType.EAGER,
            cascade = CascadeType.ALL
    )
    @JoinColumn(name = "branch_id")
    @ToString.Exclude
    private List<Employee> employees = new ArrayList<>();
    @OneToMany(
            cascade = CascadeType.ALL, orphanRemoval = true
    )
    @JoinColumn(name = "branch_id")
    @ToString.Exclude
    private List<Car> cars = new ArrayList<>();
    @OneToMany(
            cascade = CascadeType.ALL
    )
    @JoinColumn(name = "branch_id")
    @ToString.Exclude
    private List<Reservation> reservations = new ArrayList<>();


    public void addEmployee(Employee employee) {
        employees.add(employee);
    }

    public void removeEmployee(Employee employee) {
        employees.remove(employee);
    }

    public void addCar(Car car) {
        cars.add(car);
    }

    public void removeCar(Car car) {
        cars.remove(car);
    }

    public void addReservation(Reservation reservation) {
        reservations.add(reservation);
    }

    public void removeReservation(Reservation reservation) {
        reservations.remove(reservation);
    }




}
