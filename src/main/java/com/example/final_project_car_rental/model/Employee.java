package com.example.final_project_car_rental.model;

import com.example.final_project_car_rental.model.dto.EmployeeDto;
import com.fasterxml.jackson.annotation.*;
import lombok.Data;
import lombok.ToString;

import javax.persistence.*;

@Entity
@Data
@Table(name = "Employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String firstName;
    private String lastName;
    private String position;
    @ManyToOne
    @ToString.Exclude
    private Branch branch;


//    public static Employee from(EmployeeDto employeeDto) {
//        Employee employee = new Employee();
//        employee.setFirstName(employeeDto.getFirstName());
//        employee.setLastName(employeeDto.getLastName());
//        employee.setPosition(employeeDto.getPosition());
//        return employee;
//    }


}
