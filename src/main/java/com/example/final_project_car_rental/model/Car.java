package com.example.final_project_car_rental.model;

import javax.persistence.*;

import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;


@Entity
@Data
@Table(name = "Car")
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String picture;
    private String brand;
    private String model;
    private String bodyType;
    private Long year;
    private String color;
    private Long mileage;
    private String status;
    private Long amount;

    public Branch getBranch() {
        return branch;
    }

    public void setBranch(Branch branch) {
        this.branch = branch;
    }

    @ManyToOne
    @ToString.Exclude
    private Branch branch;
    @OneToMany(
            mappedBy = "car",
            cascade = CascadeType.ALL

    )
    private List<Reservation> reservations = new ArrayList<>();

    public List<Reservation> getReservations() {
        return reservations;
    }

    public void setReservations(List<Reservation> reservations) {
        this.reservations = reservations;
    }

    public void addReservation(Reservation reservation) {
        reservations.add(reservation);
    }

    public void removeReservation(Reservation reservation) {
        reservations.remove(reservation);
    }

    @Transient
    public String getCarImagePath() {
        if (picture == null || id == null) return null;

        return "/car-pictures/" + id + "/" + picture;


    }


//    public static Car from(CarDto carDto) {
//        Car car = new Car();
//        car.setBrand(carDto.getBrand());
//        car.setModel(carDto.getModel());
//        car.setBodyType(carDto.getBodyType());
//        car.setYear(carDto.getYear());
//        car.setColor(carDto.getColor());
//        car.setMileage(carDto.getMileage());
//        car.setStatus(carDto.getStatus());
//        car.setAmount(carDto.getAmount());
//        return car;
//    }


}
